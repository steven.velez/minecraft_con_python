
import mcpi.minecraft as minecraft
import mcpi.block as block

mc = minecraft.Minecraft.create()

mc.setBlocks(0,0,0,100,100,100,block.AIR) #Comando para hacer espacio para construir la casa. (cambio de la posicion)
mc.setBlocks(0,1,0,100,1,100,block.GLOWSTONE_BLOCK) #Comando para construir el piso de la casa. (luminosidad y nuevo material)

mc.setBlocks(10,2,10,20,10,20,block.NETHER_BRICK) #Comando para construir la pared. (cambio del material y color)
mc.setBlocks(11,3,11,19,9,19,block.AIR) #Comando para crear espacio dentro de la casa. (se extendio el espacio)
mc.setBlocks(14,3,10,14,4,10,block.DOOR_WOOD) #Comando para hacer la puerta de la casa. (cambio de ubicacion y material)
mc.setBlocks(13,6,10,18,8,10,block.GLASS) #Comando para hacer las ventana de la casa. (cambio de posicion y material de las ventanas)


mc.setBlocks(10,11,10,20,11,20,block.NETHER_BRICK) #nivel 1 deltecho. (material y color cambiados)

mc.setBlocks(11,12,11,19,12,19,block.NETHER_BRICK) #nivel 2 del techo. (material y tamaño cambaidos)
